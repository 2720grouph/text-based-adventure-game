# README #

Link to repository: https://bitbucket.org/2720grouph/text-based-adventure-game

Source code and Makefile located in source/ 

Doxygen documentation, User Manual, and Updated Design Proposal located in docs/. 

### What is this repository for? ###

* Documenting and storing source code for our text-based adventure game
* Super awesome text-based game version: 1.0 
