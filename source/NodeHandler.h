//************************
// NodeHandler.h
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef NODEHANDLER_H
#define NODEHANDLER_H

#include <vector>
#include <fstream>
#include "Node.h"
#include <iostream>

class NodeHandler {

public:

	NodeHandler();

	/**
	Reads from a file to create the nodes
	*/
	std::vector<Node*> createNodes(std::ifstream *is);

	/**
	Returns the current nodeID
	*/
	int getCurrentNodeId();

	/**
	Uses the list to determine the 'travel-able' options.
	During this process, it will display all node options and place
	an option number before each description. Then ask the user for the
	menu option number and return the nodeID belonging to that option.
	*/
	int displayNodes(std::vector<int> clueIds);

	/**
	Sets the current node to the node with this ID
	*/
	void setCurrentNode(int nodeID);

private:

	//*********ATTRIBUTES**********

	///This is the hallway, where all other rooms are connected to
	Node* rootNode;

	/**
	The current node we are in the tree (the location the player is
	standing). Always starts on the root node
	*/
	Node* currentNode;

	//*****************************

	/**
	Traverses the node tree to find the node of the ID
	*/
	Node* getNodebyID(int nodeID);


};
#endif //end of NodeHandler header file
