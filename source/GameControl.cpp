//************************
// GameControl.cpp
// Date: October 29, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include "GameControl.h"
#include <stdexcept>
#include "Clue.h"
#include "MinorPlotPoint.h"
#include "MajorPlotPoint.h"
#include <fstream>
#include <string>

///cpp file of GameControl

int main(){
    GameControl gc;
    gc.play();

    return 0;
}

GameControl::GameControl() {

    clueHandler = new ClueHandler();
    nodeHandler = new NodeHandler();
    isOver = false;
}

void GameControl::play() {
    try{
       //std::cout<<"AAAAAAAAAAAA"<<std::endl;
        setup();
       //std::cout<<"ZZZZZZZZZZZZ"<<std::endl;
    }catch(std::invalid_argument e){
        std::cout << "setup failed: " << e.what() << std::endl;
        return;
    }
   nodeHandler->setCurrentNode(0);  // starting node
   while (!isOver)
   {
       /* Display Clues */
        int current = nodeHandler->getCurrentNodeId();
        clueHandler->displayClues(current);
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;

       /* System Flags */
        int systemFlags = clueHandler->gatherSystemFlags();
        std::vector<int> vint;
        std::string str;
        MajorPlotPoint* mpp = new MajorPlotPoint(vint,-1,str,0);
        if(systemFlags != 0){
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        }
        if( systemFlags == mpp->saveFlag ){
       std::cout << "[ Saved ]" << std::endl;
            this->saveToFile("save.dat");
        }
        if( systemFlags == mpp->restoreFlag ){
            if( this->getResumeFile("save.dat") ){
                // saved
                std::cout << "[ Resumed ]" << std::endl;
            }else{
                // not saved
                std::cout << "[ No Resume File ]" << std::endl;
            }
        }
        if( systemFlags == mpp->exitFlag){
       std::cout << "[ Exiting ]" << std::endl;
            isOver = true;
            break;
        }
	delete mpp;

       /* Display Nodes */
        std::vector<int> clueParents = clueHandler->getActiveClueParents();
        int desiredNode = nodeHandler->displayNodes(clueParents);
        nodeHandler->setCurrentNode(desiredNode);
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
        std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
   }

}

void GameControl::setup() {
   ///Checks for files to make sure they exist
   ///3 of 4 are required. If not, then exit after pooping out an error message
   ///Required: "node.txt", "clue.txt", "load.txt"

    ///required files
   //std::cout<<"BBBBB"<<std::endl;
    if( getNodeFile("node.dat") == false){
        throw std::invalid_argument("file node.dat not found");
    }
    //std::cout<<"CCCCC"<<std::endl;
    if( getClueFile("clue.dat") == false){
        throw std::invalid_argument("file clue.dat not found");
    }
    //std::cout<<"DDDDD"<<std::endl;
    if( getInitFile("initial.txt") == false){
        throw std::invalid_argument("file initial.txt not found");
    }
    //std::cout<<"EEEEE"<<std::endl;

}


std::vector<int> GameControl::Handle_Clues(int nodeID) {

    clueHandler->displayClues(nodeID);
    std::vector<int> activeClueParents = clueHandler->getActiveClueParents();

    return activeClueParents;
}

int GameControl::handle_node() {
    return nodeHandler->displayNodes(clueHandler->getActiveClueParents());
}

bool GameControl::getNodeFile(std::string filename) {
    std::ifstream myfile;
    try{
        myfile.open("node.dat");
        myfile.close();
        nodeHandler->createNodes(&myfile);
    }catch(std::exception e){
        return false;
    }
    return true;
}

bool GameControl::getClueFile(std::string filename) {
    std::ifstream myfile;
    try{
        myfile.open("clue.dat");
        myfile.close();
        clueHandler->createClues(&myfile);
    }catch(std::exception e){
        return false;
    }
    return true;
}

bool GameControl::getResumeFile(std::string filename) {
    try{
        std::ifstream myfile;
        myfile.open("save.dat");
        std::string s;
        for(int i = 0; i < clueHandler->clueList.size(); i+=1){
            Clue* clue = clueHandler->clueList.at(i);
            myfile >> s;
            if(s == "T"){
                clue->setActive(true);
            }else{
                clue->setActive(false);
            }
            myfile >> s;
            if(s == "T"){
                clue->used = true;
            }else{
                clue->used = false;
            }
        }
        myfile.close();

        return true;
    }catch(std::exception e){
        return false;
    }
}

bool GameControl::getInitFile(std::string filename) {
    try{
        return true;
    }catch(std::exception e){
        return false;
    }
}
bool GameControl::saveToFile(std::string filename) {
    try{
        std::ofstream myfile;
        myfile.open("save.dat");
        for(int i = 0; i < clueHandler->clueList.size(); i+=1){
            Clue* clue = clueHandler->clueList.at(i);
            if(clue->isActive()){
                myfile << "T\n";
            }else{
                myfile << "F\n";
            }
            if(clue->used){
                myfile << "T\n";
            }else{
                myfile << "F\n";
            }
        }
        myfile.close();

        return true;
    }catch(std::exception e){
        return false;
    }
}

