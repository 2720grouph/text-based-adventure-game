//************************
// MinorPlotPoint.h
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef MINORPLOTPOINT_H
#define MINORPLOTPOINT_H

#include "Clue.h"

class MinorPlotPoint: public Clue {

  public:

    MinorPlotPoint(std::vector<int> clueRequired, int parentNodeId, std::string dialog);

   ///Returns an integer
   ///Will return a blank zero
   int getSystemFlag();

   /**
   Receives a message about activated clue and checks to see if it is active
   */
   void notify(int clueID);

   /**
   this integer is always equal to zero
   */
   static const int blankFlag = 0;


};

#endif ///end of MinorPlotPoint header file
