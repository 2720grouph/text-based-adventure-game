//************************
// MajorPlotPoint.h
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef MAJORPLOTPOINT_H
#define MAJORPLOTPOINT_H

#include "Clue.h"

class MajorPlotPoint: public Clue {

  public:

/**
 * constructor for MajorPlotPoint
 */
MajorPlotPoint(std::vector<int> cluesrequired, int parentnodeid, std::string dialog, int trig);


   /**
   If active, then will set current flag to trigger flag
   */
   void notify(int clueID);

   /**
   Returns the current flag
   */
   int getSystemFlag();


   //*******ATTRIBUTES********

   static const int saveFlag = 2;

   static const int restoreFlag = 4;

   static const int exitFlag = 8;

  private:

   //*************************

   /**
   Will keep the flag around for when the clue is activated
   */
   void setTrigger(int triggerFlag);

};

#endif ///end of MajorPlotPoint header file
