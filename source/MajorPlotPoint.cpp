//************************
// MajorPlotPoint.cpp
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include "MajorPlotPoint.h"

/**
 * constructor for MajorPlotPoint
 */
MajorPlotPoint::MajorPlotPoint(std::vector<int> cluesrequired, int parentnodeid, std::string dialog, int trig)
    :Clue(cluesrequired, parentnodeid, dialog){
    this->trigger = trig;
    this->currentFlags = 0;
}


void MajorPlotPoint::notify(int clueID) {

   /**
      ClueID is the activated clue being notified about
      We will use the Observer pattern to receive messages about activated clue
      If clueID is contained in the required clues list, then set triggerflag
      to currentflags
   */

   for (int i = 0; i < requiredClues.size(); i++) {

      ///Check to see if clueID exists in MajorPlotPoint's required clues list
      if (requiredClues.at(i) == clueID && !used)
      {
	 ///Set the clue to active
	 setActive(true);
      }
   }

}

int MajorPlotPoint::getSystemFlag() {

   return currentFlags;

}

void MajorPlotPoint::setTrigger(int triggerFlag) {

   trigger = triggerFlag;

}

