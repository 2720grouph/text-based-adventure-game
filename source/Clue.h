//************************
// Clue.h
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef CLUE_H
#define CLUE_H

#include <vector>
#include <iostream>
#include <string>

class Clue {

  public:

   //~Clue();

   /**
   Initialize IDS  of clues to become active and where to find them
   */
   Clue(std::vector<int> cluesrequired, int parentnodeid, std::string dialog);

   /**
   Receiving a message about activated clue, and checks to see if it is active
   */
   virtual void notify(int clueID) = 0;

   /**
   Displays dialogue
   */
   void display();
   /**
   Return if it's active
   */
   bool isActive();

   /**
   Checks to see if the parent is the node
   */
   bool isChildofNode(int nodeID);

   /**
   Overriden by majorPlotPoint and minorPlotPoint
   */
   virtual int getSystemFlag() = 0;

   /**
   Returns parent nodeID
   */
   int getParentNodeId();

   /**
   Needed to continue from a saved state
   */
   bool setActive(bool active);

   //*******ATTRIBUTES*******

   ///when the clue is read it turns it's self off so that it will not display again
   bool used;

   int currentFlags;

   int trigger;

  protected:

   ///What to display to the screen
   std::string dialogue;

   ///IDs of clues required to become active
   std::vector<int>requiredClues;

   ///Where to find the clue. This is the parent node's
   int parentNodeID;

   ///Its active state, default is false. We will turn it on during setup
   bool active;


   //*************************



};

#endif ///end of Clue header file
