//************************
// Clue.cpp
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include <vector>
#include "Clue.h"

///cpp file of Clue

Clue::Clue(std::vector<int> cluesrequired, int parentnodeid, std::string dialog){
    this->requiredClues = cluesrequired;
    this->parentNodeID = parentnodeid;
    this->dialogue = dialog;
    this->active = false;
    this->used = false;
    this->currentFlags = 0;
    trigger = 0;
}

//Clue::~Clue(){}

//Clue::Clue(){}

void Clue::display()
{

   std::cout << dialogue;
    if(trigger == 0){
        used = true;
    }

    this->currentFlags = this->trigger;
}

bool Clue::isActive() {return active;}

int Clue::getParentNodeId() {return parentNodeID;}

bool Clue::setActive(bool setactive) {active = setactive;return setactive;}

bool Clue::isChildofNode(int nodeID)
{
    return(nodeID == parentNodeID);
}


///end of Clue cpp file
