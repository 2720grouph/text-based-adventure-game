//************************
// GameControl.h
// Date: October 29, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef GAMECONTROL_H
#define GAMECONTROL_H

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include "ClueHandler.h"
#include "NodeHandler.h"


///Header file of GameControl

class GameControl {

  public:

   /**
   Initialize ClueHandler and NodeHandler
   */
   GameControl();

   /**
   Contains game loop
   */
   void play();

  private:

   //******ATTRIBUTES******
   ClueHandler* clueHandler;

   NodeHandler* nodeHandler;

   bool isOver;
   //**********************


   ///Pass the files to the Node and Clue handlers to load data

   void setup();

   /**
   Calls the clue handler to display the currently found clues at the location (nodeID)
   and then notifies the other clues that they have been found
	*/
   std::vector<int> Handle_Clues(int nodeID);

   /**
   Calls the node handler to traverse the nodes (moves the player) and return the nodeID (current location)
   when it's done
   */
   int handle_node();

   /**
   Check for any existing node file
   */
   bool getNodeFile(std::string filename);

   /**
   Check for any existing clue file
   */
   bool getClueFile(std::string filename);

   /**
   Check for an existing saved file
   */
   bool getResumeFile(std::string filename);

   /**
   Check for an initial state file
   */
   bool getInitFile(std::string filename);

   /**
   Prints the state of the clues and/or nodes
   */
   bool saveToFile(std::string filename);


};

#endif ///end of gameControl header file

