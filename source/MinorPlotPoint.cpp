//************************
// MinorPlotPoint.cpp
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include "MinorPlotPoint.h"

///cpp file for MinorPlotPoint

/**
 * constructor for minorPlotPoint
 */
MinorPlotPoint::MinorPlotPoint(std::vector<int> cluesrequired, int parentnodeid, std::string dialog)
    :Clue(cluesrequired,parentnodeid,dialog){
}

int MinorPlotPoint::getSystemFlag() {

   return 0;
}

void MinorPlotPoint::notify(int clueID) {

   for (int i = 0; i < requiredClues.size(); i++) {

	/**
	Check to see if clueID exists in MajorPlotPoint's required clues list
    */
	if (requiredClues.at(i) == clueID && !used)
    {
	///Set the clue to active
	 setActive(true);
	///Set currentFlags to trigger
	 //currentFlags = trigger;
    }
   }


}


///end of cpp MinorPlotPoint

