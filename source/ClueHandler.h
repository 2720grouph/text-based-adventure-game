//************************
// ClueHandler.h
// Date: October 29, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef CLUEHANDLER_H
#define CLUEHANDLER_H

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include "Clue.h"

///Header file of ClueHandler

class ClueHandler {

  public:

   /**
   Initialize variables in order to prepare for loading the clues
   */
   ClueHandler();

   /**
   Displays active clues returned from getActiveClues, also calls notify as the clues get
   triggered/displayed
   */
   void displayClues(int nodeID);

   ///Reads from an input stream to create the nodes
   void createClues(std::istream *is);

   ///Notifies every other clue it has been triggered
   void notifyClues(int clueID);

   ///Gathers majorPlotPoint flags assigned to specific integers
   int gatherSystemFlags();

   /**
   Returns compiled list of crrent active clue parents. Useful to the NodeHandler to know if a
   node could be traveled to.
   */
   std::vector<int> getActiveClueParents();

   ///Runs down list an sets the state of every clue. Useful for a new game and a resume
   void setClueStates(std::istream in);

   ///Prints the states to the output stream
   void getClueStates(std::ostream out);





   //******ATTRIBUTES******

   std::vector<Clue*> clueList;

   //**********************
  private:


   ///Adds clue to clueList
   void registerClue(Clue *ptr);


   ///Search for a clue by its ID
   Clue* getClueByID(int clueID);

   ///Compiles a list of active clues under the NodeID
   std::vector<Clue*> getActiveClues(int nodeID);



};

#endif ///end of ClueHandler header file

