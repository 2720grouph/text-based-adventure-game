
//************************
// NodeHandler.cpp
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include "NodeHandler.h"
#include<iostream>
#include<vector>
#include<limits>
#include <stdexcept>


NodeHandler::NodeHandler() {}

int NodeHandler::getCurrentNodeId()
{
	return currentNode->getNodeID();
}

int NodeHandler::displayNodes(std::vector<int> clueIds)
{
	int selectedNodeOption;
/*	if (!clueIds.empty())
	{
		for (int i = 0; i < clueIds.size();++i)
		{
			Node* n = getNodebyID( clueIds.at(i) );
			int nId = n->getNodeID();
			std::cout << nId << ": ";
			n->display();
		}
	}*/

	//display options
    if(getCurrentNodeId() == 0){
         // at root
    }else if(currentNode->parentNodeID == 0){
        // hallway is the parent
		std::cout << currentNode->parentNodeID << ": ";
        getNodebyID(currentNode->parentNodeID)->display();
    }else{
        //display parent and the hallway
		std::cout << 0 << ": ";
        getNodebyID(0)->display();
		std::cout << currentNode->parentNodeID << ": ";
        getNodebyID(currentNode->parentNodeID)->display();
    }
    //display active children
    for(int i=0; i<currentNode->childNodes.size(); i+=1){
        Node* child = currentNode->childNodes.at(i);
        bool onlist = false;
        for(int j=0; j<clueIds.size(); j+=1)
            if(clueIds.at(j) == child->getNodeID())
                onlist = true;
        if(onlist){
            std::cout << child->nodeID << ": ";
            child->display();
        }
    }


	//std::cout << "111: Help" << std::endl;;
	bool helpfired = false;
	while(true){
        std::cout << "Please choose an option (enter an option #, 0 for hallway, or 111 for Help): ";
        std::cin >> selectedNodeOption;
        ///gets user input
        while (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Bad entry.Please enter a NUMBER: ";
            std::cin >> selectedNodeOption;
        }
        ///Help option for player
        if(selectedNodeOption==111 && helpfired == false)
        {
	   helpfired = true;
           std::cout << '\n';
           std::cout<< "In front of you is the console. On the console are options refuring to the rooms and actions a player can perform. Type one of the displayed numbers into the console and press enter. One of the options should be performed such as entering a room and checking under the bed. When you perform an action it can lead to a clue. A clue is a piece of dialog that tells you more of the story. This is designed like a 'choose-your-own-adventure' book where you pick the action of the player. These clues unlock more clues to learn more. After sufficient clues are found, dialog will be available to end the game. You can win or lose. Your choice. Good luck." <<std::endl<<std::endl;
           //displayNodes(clueIds);
        }
        ///Check if the input is valid
        for (int i = 0; i < clueIds.size();++i)
        {
           if(clueIds[i]==selectedNodeOption || 0==selectedNodeOption){
	      // check if the selected node is a neigbor
		bool neibor = false;
		if(getNodebyID(selectedNodeOption)->getNodeParent() == currentNode->getNodeID())
			neibor = true;
		if(currentNode->getNodeParent() == selectedNodeOption || 0==selectedNodeOption)
			neibor = true;
		if(neibor == true)
              		return selectedNodeOption;
	   }
        }
        std::cout << "Please choose a valid option.";
	}

}


void NodeHandler::setCurrentNode(int nodeID)
{
	currentNode = getNodebyID(nodeID);
}

Node* NodeHandler::getNodebyID(int nodeID)
{
    return rootNode->getChild(nodeID);
}

std::vector<Node*> NodeHandler::createNodes(std::ifstream *is){
    std::vector<Node*> nodelist;

    std::ifstream myfile;
    try{
        myfile.open("node.dat");
    }catch(std::exception e){
        return nodelist;
    }
    //std::ifstream myfile = *is;
    int id;
    int parent;
    std::string display;
    std::string line;

    try{
        while(! (myfile.eof()) ){
        //while(false){

            //id
            myfile >> line;
            line = line.substr(3);
            line.erase(line.end()-1);
            sscanf(line.c_str(), "%d", &id);

            //ingore node name
            myfile >> line;

            //parent node
            myfile >> line;
            line = line.substr(13);
            line.erase(line.end()-1);
            sscanf(line.c_str(), "%d", &parent);

            //display
            myfile >> line;
            line = line.substr(8);
            display = line;
            char c = ' ';
            do{
                c = myfile.get();
                display+=c;
            }while(c != ';');
            display.erase(display.end()-1);

            //create nodes!
            Node* node;
            //find its parent
            if(parent == 999){
                //is root node
                node = new Node(display,parent,id);
                rootNode = node;
            }else{
                //has a parent
                //find the parent
                int parentIndex;
                for(int i = 0; i < nodelist.size(); i += 1){
                    if(nodelist.at(i)->getNodeID() == parent){
                        parentIndex = i;
                    }
                }
                nodelist.push_back(   nodelist.at(parentIndex)->createChildNode(display, parent, id)   );
            }
            nodelist.push_back(node);
        }
        return nodelist;
    }catch(std::exception e){
        return nodelist;
    }
    return nodelist;
nodelist.clear(); 
}

//end if NodeHandler cpp file
