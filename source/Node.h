//************************
// Node.h
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#ifndef NODE_H
#define NODE_H

#include<vector>
#include<iostream>
#include <string>

class Node {

public:
	/**
	Constructor.
	Also called by createChildNode as we add nodes by calling to its parent, or when
	creating the root node
	*/
	Node(std::string des, int parentNode, int node);

	/**
	This looks through the child nodes for the matching nodeID and returns it
	Return NULL if not found. Also asks for child nodes to find it and
	returns their node if they found the node.
	*/
	Node* getChild(int nodeID);

	/**
	Will create a node with these inputs and attach it to the child list of this node
	*/
	Node* createChildNode(std::string description, int parentNodeID, int nodeID);


	/**
	Returns the ID of the node
	*/
	int getNodeID();

	/**
	Returns the node's parentID
	*/
	int getNodeParent();

	/**
	Will write description to screen
	*/
	void display();


	//**********ATTRIBUTES*************

	//identifies the parentNode
	int parentNodeID;

	//Id to identify this node
	int nodeID;

	//Contains the child nodes
	std::vector<Node*> childNodes;
	private:

	//Holds description, acts as an option menu of where to go
	std::string description;

	//*********************************


};
#endif //end of Node header file



