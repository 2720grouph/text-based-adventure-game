
//************************
// Node.cpp
// Date: October 30, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include "Node.h"
#include <iostream>
#include <string>
#include <iterator>
/**
Constructor.
Also called by createChildNode as we add nodes by calling to its parent, or when
creating the root node
*/
Node::Node(std::string des, int parentNode, int node)
{

	description = des;
	parentNodeID = parentNode;
	nodeID = node;

}

Node* Node::getChild(int paramNodeID)
{
    if(this->nodeID == paramNodeID)return this;
    else{
        if(childNodes.empty())return NULL;
        else{
            for(int i=0; i<childNodes.size();i+=1){
                Node* childnode = childNodes.at(i);
                Node* resultnode = childnode->getChild(paramNodeID);
                if(resultnode != NULL)return resultnode;
            }
            return NULL;
        }
    }
}

/**
Will create a node with these inputs and attach it to the child list of this node
*/
Node* Node::createChildNode(std::string description, int parentNodeID, int nodeID)
{
	Node* c = new Node(description, parentNodeID, nodeID);
	childNodes.push_back(c);
	return c;
}

/**
Returns the ID of the node
*/
int Node::getNodeID()
{
	return nodeID;
}

/**
Returns the node's parentID
*/
int Node::getNodeParent()
{
	return parentNodeID;
}

/**
Will write description to screen
*/
void Node::display()
{
	std::cout << description << std::endl;
}



///end of cpp Node file
