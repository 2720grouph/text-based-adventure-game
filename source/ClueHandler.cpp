//************************
// ClueHandler.cpp
// Date: October 29, 2015
// Authors: Fazla Chowdhury, Jefferson Sylvia-Iriogbe, Robin Vanderkooi, Micah Tigley
//************************

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "ClueHandler.h"
#include "Clue.h"
#include "MinorPlotPoint.h"
#include "MajorPlotPoint.h"
#include <limits>

///cpp file of ClueHandler

ClueHandler::ClueHandler() {}

void ClueHandler::displayClues(int nodeID) {

/*    std::vector<Clue*> activeClues = getActiveClues(nodeID);
   for (int i = 0; i < activeClues.size() ; i++){
        if(activeClues.at(i)->getParentNodeId() == nodeID){  /// displaying clues under the current node
            activeClues.at(i)->display();

            ///will have to find the clue's index on the big list and use that as a clue id as the index on the reduced list will be different
            for(int j=0; j<clueList.size();++j){
                if(clueList.at(j) == activeClues.at(i))///found same item
                    notifyClues(j); /// notify the rest
            }
        }
   }
   ///Call notifyClues(int ClueID)
*/
    std::string s;
    for(int i=0; i<clueList.size();i+=1){
        Clue* clue = clueList.at(i);
        if(clue->isActive() && !(clue->used) && clue->getParentNodeId() == nodeID){
            clue->display();

            std::cin.get();
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
            for(int j=0; j<clueList.size();j+=1){
                Clue* notifyClue = clueList.at(j);
                notifyClue->notify(i);
            }
        }
    }

}

void ClueHandler::createClues(std::istream *is) {
    std::vector<Clue*> cluelist;

    //open file
    std::ifstream myfile;
    try{
        myfile.open("clue.dat");
    }catch(std::exception e){
        std::cout << "NO clue.dat FILE!";
        return;
    }

    //components needed to build clues
    int id;
    int parent;
    std::vector<int> required;
    std::string trigger;
    std::string dialogue;

    std::string line;

    try{
        while(! (myfile.eof()) ){
        //while(false){

            //id
            myfile >> line; // ignore
            myfile >> line;
            line = line.substr(7);
            line.erase(line.end()-1);
            sscanf(line.c_str(), "%d", &id);

            //ingore parent name name
            myfile >> line;

            //parent node
            myfile >> line;
            line = line.substr(9);
            line.erase(line.end()-1);
            sscanf(line.c_str(), "%d", &parent);

            //required clue ids - we will have to parse a possible vector
            myfile >> line; // skip names
            char c = 'y';
            int tempi;
            do{c = myfile.get();}while(c != ':'); // puts us in the starting pos
            while(c != ';'){
                line = "";
                do{
                    c = myfile.get();
                    line += c;
                }while(c != ';' && c != ','); //  next token
                if(line == ";"){ // nothing was found
                    // clue is active as it has no requirements
                }else if(c == ','){ // id found and more to come
                    line.erase(line.end()-1);
                    sscanf(line.c_str(), "%d", &tempi);
                    required.push_back(tempi);
                }else if(c == ';'){ // last id found.
                    line.erase(line.end()-1);
                    sscanf(line.c_str(), "%d", &tempi);
                    required.push_back(tempi);
                }
            }

            //trigger
            myfile >> line;
            line = line.substr(8);
            line.erase(line.end()-1);
            trigger = line;

            //dialogue
            do{c = myfile.get();}while(c != ':'); // puts us in the starting pos
            line = "";
            do{c = myfile.get(); line+=c;}while(c != ';'); // reads until the ending token
            line.erase(line.end()-1);
            dialogue = line;

            //create a clue!
            if(trigger == "blank"){
                //minor plot point
                this->clueList.push_back( new MinorPlotPoint(required,parent,dialogue) );
            }else{
                //major plot point
                int trig = 0;
                MajorPlotPoint *mpp;
                if(trigger == "save") trig = mpp->saveFlag;
                if(trigger == "exit") trig = mpp->exitFlag;
                if(trigger == "resume") trig = mpp->restoreFlag;
                this->clueList.push_back( new MajorPlotPoint(required,parent,dialogue,trig));
            }

            //set active status
            if(required.empty())clueList.back()->setActive(true);

            //reset vectors
            required.clear();
	    //std::cout<<"GGGGG"<< id <<std::endl;

            //eject
            if(id == 33){
                    break;
            }
        }
        return;
    }catch(std::exception e){
        return;
    }
    return;
}

void ClueHandler::notifyClues(int clueID) {
    for(int i =0; i<clueList.size(); ++i){
        clueList.at(i)->notify(clueID);
    }
}

int ClueHandler::gatherSystemFlags() {
    int finalFlag = 0;
    for(int i =0; i<clueList.size(); ++i){
        int k = clueList.at(i)->getSystemFlag();
        if(k!=0){
            finalFlag = k;
            clueList.at(i)->currentFlags = 0;
        }
    }
    return finalFlag;
}

std::vector<int> ClueHandler::getActiveClueParents() {
    std::vector<Clue*> activeClues; ///all, not just the ones related to the current nodeId
    for(int i = 0; i<clueList.size(); ++i){
        if(clueList.at(i)->isActive())
            activeClues.push_back(clueList.at(i));
    }

    std::vector<int> clueParents;
    for(int i = 0; i<activeClues.size(); ++i){
        clueParents.push_back(activeClues.at(i)->getParentNodeId());
    }

    return clueParents;
}

void ClueHandler::setClueStates(std::istream in) {
    ///TODO will fill this out when first of the written clues are created
}

void ClueHandler::getClueStates(std::ostream out) {
    ///TODO will fill this out when first of the written clues are created
}

void ClueHandler::registerClue(Clue* ptr) {
    clueList.push_back(ptr);
}

Clue* ClueHandler::getClueByID(int clueID) {
    return clueList.at(clueID);
}

std::vector<Clue*> ClueHandler::getActiveClues(int nodeID) {
    std::vector<Clue*> activeClues;
    for(int i=0; i<clueList.size(); ++i){
        if(clueList.at(i)->isActive())
            if(clueList.at(i)->isChildofNode(nodeID))
                activeClues.push_back(clueList.at(i));
    }
    return activeClues;
}


///end of ClueHandler cpp file

